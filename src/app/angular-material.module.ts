import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatButtonModule, MatToolbarModule, MatNativeDateModule, MatTabsModule,
    MatIconModule, MatSidenavModule, MatListModule, MatCardModule, MatSnackBarModule,
    MatInputModule, MatProgressSpinnerModule, MatPaginatorModule, MatDialogModule, MatFormFieldModule, MatSelectModule, MatRadioModule
  ],
  exports: [
    CommonModule,
    MatButtonModule, MatToolbarModule, MatNativeDateModule, MatTabsModule,
    MatIconModule, MatSidenavModule, MatListModule, MatCardModule, MatSidenavModule, MatListModule, MatSnackBarModule,
    MatInputModule, MatProgressSpinnerModule, MatPaginatorModule, MatDialogModule, MatFormFieldModule, MatSelectModule, MatRadioModule
  ]
})
export class AngularMaterialModule { }
