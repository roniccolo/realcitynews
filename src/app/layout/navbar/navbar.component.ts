import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isLoggedIn = false;

  constructor() { }

  ngOnInit(): void {
  }

  loginDialog() {
    console.log('please login');
  }

  onLogout() {
    console.log('you are now Log out!');
  }

}
